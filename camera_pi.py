import time
import io
import threading
import picamera
import os
import os.path

class Camera(object):
	thread = None  # background thread that reads frames from camera
	frame = None  # current frame is stored here by background thread
	last_access = 0  # time of last client access to the camera
	camera_stop = False # false means camera IS RUNNING

	def initialize(self):
		if Camera.thread is None:
			Camera.camera_stop = False
			# start background frame thread
			Camera.thread = threading.Thread(target=self._thread)
			Camera.thread.start()

			# wait until frames start to be available
			while self.frame is None:
				time.sleep(0)

	def take_picture(self):
		try:
			os.remove('static/foo.jpg')
		except OSError:
			pass
		if not Camera.camera_stop:
			Camera.camera_stop = True
		with picamera.PiCamera() as camera:
			camera.resolution = (1024, 768)
			# let camera warm up
			camera.start_preview()
			time.sleep(2)
			camera.capture('static/foo.jpg')
		return "<img src='static/foo.jpg' />"

	def get_frame(self):
		Camera.last_access = time.time()
		self.initialize()
		return self.frame

	def camstop(self):
		Camera.camera_stop = True
		return "stopped"

	def pause(self):
		if Camera.camera_stop:  # if true then camera is stopped
			Camera.camera_stop = False
			return self.get_frame()
		else:  # camera is running so lets stop it
			Camera.camera_stop = True
			return ""

	@classmethod
	def _thread(cls):
		with picamera.PiCamera() as camera:
			# camera setup
			camera.resolution = (320, 240)
			camera.hflip = True
			camera.vflip = True

			# let camera warm up
			camera.start_preview()
			time.sleep(2)

			stream = io.BytesIO()
			for foo in camera.capture_continuous(stream, 'bmp', use_video_port=True):
				# store frame
				stream.seek(0)
				cls.frame = stream.read()

				# reset stream for next frame
				stream.seek(0)
				stream.truncate()

				# if there hasn't been any clients asking for frames in
				# the last 10 seconds stop the thread
				if time.time() - cls.last_access > 10 or Camera.camera_stop:
					break
		cls.thread = None
