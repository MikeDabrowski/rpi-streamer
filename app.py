from flask import Flask, render_template, Response
from camera_pi import Camera

app = Flask(__name__)

@app.route('/')
def index():
	return render_template('index.html', cam_stop=cam_stop)

def gen(camera):
	"""Video streaming generator function."""
	while True:
		frame = camera.get_frame()
		yield (b'--frame\r\n'
		       b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')

cam_stop = False

@app.route('/video_feed')
def video_feed():
	"""Video streaming route. Put this in the src attribute of an img tag."""
	return Response(gen(Camera()), mimetype='multipart/x-mixed-replace; boundary=frame')

@app.route('/startStop/start')
def stop_vid():
	return Camera().pause(), 204

@app.route('/picture')
def picture():
	Camera().take_picture()
	return render_template('vf.html', iimage='static/foo.jpg')

if __name__ == '__main__':
	app.run(host='0.0.0.0', debug=True, threaded=True)
